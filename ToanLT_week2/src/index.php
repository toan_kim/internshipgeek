<?php
use App\Router;

require_once __DIR__ . '/vendor/autoload.php';

const BASE_PATH = __DIR__;
session_start();
$dotenv = \Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

function runMigrations()
{
    $files = glob('App/Migrations/*');
    foreach ($files as $file) {
        require_once $file;
    }
}

runMigrations();
$route = new Router();
$route->dispatch();