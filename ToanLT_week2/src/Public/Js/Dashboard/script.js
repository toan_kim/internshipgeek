$(document).ready(function () {
    this.validateDevice = () => {
        mac = $('#addDevice .mac').val();
        add = window.tableData;
        for (let i = 0 ; i < add.length;i++){
            if (mac == add[i].mac_address) {
                return false;
            }
        }
        return true;
    }

    function renderChart() {
        function getNames() {
            let result = [];
            window.tableData.forEach(item => {
                result.push(item['name']);
            });
            return result;
        }

        function getPowers() {
            let result = [];
            window.tableData.forEach(item => {
                result.push(item['power']);
            });
            return result;
        }

        function getRandomColors() {
            let result = [];
            window.tableData.forEach(item => {
                function getRandomColor() {
                    var letters = '0123456789ABCDEF';
                    var color = '#';
                    for (var i = 0; i < 6; i++) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                }
                result.push(getRandomColor());
            });
            return result;
        }
        var xValues = getNames();
        var yValues = getPowers();
        var barColors = getRandomColors();
        new Chart("myChart", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "Power Consumption"
                }
            }
        });
    }

    function renderTable() {
        var columns = {
            'name': 'Device',
            'mac_address': 'Mac Address',
            'IP': 'IP',
            'created_at': 'Created Date',
            'power': 'Power Consumption (Kw/H)'
        }
        
        var table = $('.table-js').tableSortable({
            element: '',
            data: window.tableData,
            columns: columns,
            sorting: true,
            pagination: true,
            paginationContainer: null,
            rowsPerPage: 10,
            formatCell: null,
            formatHeader: null,
            searchField: '#searchField', // selector of search field
            responsive: {}, // specify options for different screen sizes
            totalPages: 0,
            sortingIcons: {
                asc: '<span>▼</span>',
                desc: '<span>▲</span>',
            },
            nextText: '<span>Next</span>',
            prevText: '<span>Prev</span>',
            tableWillMount: () => {},
            tableDidMount: () => {},
            tableWillUpdate: () => {},
            tableDidUpdate: () => {},
            tableWillUnmount: () => {},
            tableDidUnmount: () => {},
            onPaginationChange: null,
        })
    }

    renderTable();
    renderChart();

    $('form#addDevice').on('submit', function (e) {
        e.preventDefault();
        if(this.validateDevice()) {
            var formData = new FormData($('form#addDevice')[0]);
            $('#error').hide();
            $.ajax({
                type: "POST",
                url: '/Device/Add',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    window.tableData = JSON.parse(data);
                    renderTable();
                    renderChart();
                },
                error: function (data, textStatus, jqXHR) {
                    alert("Fail");
                }
            });
        } else {
            $('#error').show();
        }


    }.bind(this))
});
