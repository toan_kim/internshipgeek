var columns = {
    'device_id': 'Device Id',
    'name': 'Name',
    'action': 'Action',
    'date': 'Date',
}

var table = $('.table-sortable').tableSortable({
    element: '',
    data: window.logTableData,
    columns: columns,
    sorting: true,
    pagination: true,
    paginationContainer: null,
    rowsPerPage: 10,
    formatCell: null,
    formatHeader: null,
    searchField: '#searchField', // selector of search field
    responsive: {}, // specify options for different screen sizes
    totalPages: 0,
    sortingIcons: {
        asc: '<span>▼</span>',
        desc: '<span>▲</span>',
    },
    nextText: '<span>Next</span>',
    prevText: '<span>Prev</span>',
    tableWillMount: () => {},
    tableDidMount: () => {},
    tableWillUpdate: () => {},
    tableDidUpdate: () => {},
    tableWillUnmount: () => {},
    tableDidUnmount: () => {},
    onPaginationChange: null,
})