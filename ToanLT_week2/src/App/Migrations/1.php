<?php

use App\Model\Database;

$db = new Database();

$db->query("
    CREATE TABLE IF NOT EXISTS User(
        Id int(10) unsigned NOT NULL AUTO_INCREMENT,
        Username varchar(255) DEFAULT NULL,
        Password varchar(255) DEFAULT NULL,
        primary key(Id),
        UNIQUE KEY `Username` (`Username`)
    )");


    $db->query("
    CREATE TABLE IF NOT EXISTS Device(
        id int(10) unsigned NOT NULL AUTO_INCREMENT,
        name varchar(255) DEFAULT NULL,
        mac_address varchar(255) DEFAULT NULL,
        IP varchar(255) DEFAULT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        power int(10) DEFAULT NULL,
        primary key(Id)
    )");

    $db->query("
    CREATE TABLE IF NOT EXISTS Logs(
        id int(10) unsigned NOT NULL AUTO_INCREMENT,
        device_id int(10) DEFAULT NULL,
        name varchar(255) DEFAULT NULL,
        action varchar(255) DEFAULT NULL,
        date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        primary key(id)
    )");

$name = ['TV', 'Washer', 'Fan', 'Mobile', 'Laptop'];
$action = ['Turn on', 'Sleep', 'Turn off'];

for ($i=0 ; $i<10; $i++) {
    $db->insert('Logs', [
        'device_id' => rand(0,1000),
        'name' => $name[rand(0,4)],
        'action' => $action[rand(0,2)]
    ]);
}

unset($db);
