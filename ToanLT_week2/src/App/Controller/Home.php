<?php
namespace App\Controller;

class Home extends AbstractController
{
    public function execute()
    {
        if ($this->getUser()) {
            $this->redirect('/dashboard');
        } else {
            $this->render('home.php');
        }
    }
}
