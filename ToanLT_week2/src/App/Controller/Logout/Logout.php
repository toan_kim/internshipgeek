<?php
namespace App\Controller\Logout;

use App\Controller\AbstractController;

class Logout extends AbstractController
{
    public function execute()
    {
        $_SESSION['user_id'] = 0;
        $this->redirect('/home');
    }
}
