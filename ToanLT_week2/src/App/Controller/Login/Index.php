<?php
namespace App\Controller\Login;

use App\Controller\AbstractController;
use App\Model\User;

class Index extends AbstractController
{
    public function execute()
    {
        $this->addCss('Login/login.css');
        $this->addJs('Login/login.js');
        $this->setTitle("Login form");

        if (!empty($_POST['username']) && !empty($_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user = new User();
            if ($user->login($username, $password)) {
                $this->redirect('/dashboard');
            } else {
                $this->addError("You have entered the wrong account or password!");
                $this->render('Login/Index.php');
            }
        }
        else if (!$this->getUser()) {
            $this->render('Login/Index.php');
        } else {
            $this->redirect('/dashboard');
        }
    }
}