<?php
namespace App\Controller\Logs;

use App\Controller\AbstractController;
use App\Model\Logs;

class Index extends AbstractController
{
    public function execute()
    {
        $this->addCss('Dashboard/Dashboard.css');
        $this->addJs('Dashboard/app.js');
        $this->addJS('table-sortable.js');
        $this->addJS('Logs/index.js');

        $logs = new Logs();
        $tableData = json_encode($logs->getAll());
        $this->render('Logs/Index.php', ['tableData' => $tableData]);
    }
}
