<?php
namespace App\Controller\Dashboard;

use App\Controller\AbstractController;
use App\Model\Device;

class Index extends AbstractController
{
    public function execute()
    {
        if (!$this->getUser()) {
            $this->redirect('/login');
        }
        $this->addCss('Dashboard/Dashboard.css');
        $this->addJs('table-sortable.js');
        $this->addJs('Dashboard/script.js');
        $this->addJs('Dashboard/app.js');
        $device = new Device();
        $tableData = json_encode($device->getAll());
        $this->render('Dashboard/index.php', ['tableData' => $tableData]);
    }
}
