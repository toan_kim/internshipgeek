<?php

namespace App\Controller;

use App\Model\User;

abstract class AbstractController
{

    const BASE_TEMPLATE = 'base.php';
    protected $css = [];
    protected $js = [];
    protected $title = 'Document';
    protected $errors = [];

    abstract function execute();

    public function render(string $file, $data = [])
    {
        $content = $file;
        $cssFiles = $this->css;
        $jsFiles = $this->js;
        $title = $this->title;
        $errors = $this->errors;
        $app = (object) [
            'user' => $this->getUser(),
            'data' => $data
        ];

        require __DIR__ . '/../View/' . self::BASE_TEMPLATE;
    }

    public function addCss(string $cssFile)
    {
        $cssFile = '/Public/Css/'. $cssFile;
        if (!in_array($cssFile, $this->css)) {
            $this->css[] = $cssFile;
        }
    }

    public function addJs(string $jsFile)
    {
        $jsFile = '/Public/Js/'. $jsFile;
        if (!in_array($jsFile, $this->js)) {
            $this->js[] = $jsFile;
        }
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getUser()
    {
        $user = new User();
        return $user->getCurrentUser();
    }

    public function redirect($url)
    {
        header('Location: '. $url);
        exit();
    }

    public function addError($error)
    {
        $this->errors[] = $error;
    }

    public function responseJson($data)
    {
        echo json_encode($data);
        exit();
    }
}
