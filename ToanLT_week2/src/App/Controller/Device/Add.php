<?php
namespace App\Controller\Device;

use App\Controller\AbstractController;
use App\Model\Device;

class Add extends AbstractController
{
    protected function getClientIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function execute()
    {
        $name = $_POST['name'];
        $macAddress = $_POST['mac_address'];
        $ip = $this->getClientIp();
        $device = new Device();
        $power = rand(1, 100);
        $device->add($name, $macAddress, $ip, $power);
        $all = $device->getAll();
        return $this->responseJson($all);
    }
}