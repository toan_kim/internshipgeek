<?php
namespace App\Controller\Account\Create;

use App\Controller\AbstractController;
use App\Model\User;

class Add extends AbstractController
{
    public function execute()
    {
        try {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user = new User();
            $user->create($username, $password);
            $user->login($username, $password);
            $this->redirect('/dashboard');
        } catch (\Throwable $e) {
            $this->redirect('/account/create');
        }
    }
}
