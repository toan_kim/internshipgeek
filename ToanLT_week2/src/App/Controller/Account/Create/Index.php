<?php
namespace App\Controller\Account\Create;

use App\Controller\AbstractController;

class Index extends AbstractController
{
    public function execute()
    {
        $this->addCss('Account/create.css');
        $this->render('Account/Create/Index.php');
    }
}
