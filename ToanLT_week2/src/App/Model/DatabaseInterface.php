<?php
namespace App\Model;

interface DatabaseInterface
{
    /**
     * @param string $table
     * @param array $data
     * @return bool|string
     */
    public function insert(string $table, array $data);

    /**
     * @param string $table
     * @param array $data
     * @param string|null $where
     * @return int
     */
    public function update(string $table, array $data, string $where = null);

    /**
     * @param string $table
     * @param string $where
     * @return int
     */
    public function delete(string $table, string $where);

    /**
     * @param string $table
     * @param string|null $where
     * @return bool|array
     */
    public function get(string $table, string $where = null);
}