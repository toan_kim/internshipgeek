<?php

namespace App\Model;

use App\Model\Database;

class Logs
{
    const TABLE = 'Logs';
    protected $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getAll()
    {
        return $this->db->get(self::TABLE);
    }
}