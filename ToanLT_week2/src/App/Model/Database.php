<?php

namespace App\Model;

use PDO;
use PDOException;

class Database implements DatabaseInterface
{
    private $conn;

    /**
     * @param string $host
     * @param string $dbname
     * @param string $username
     * @param string $password
     */
    public function __construct()
    {
        $host = getenv('DB_HOST');
        $dbname = getenv('DB_NAME');
        $username = getenv('DB_USERNAME');
        $password = getenv('DB_PASSWORD');

        $dsn = "mysql:host=$host;dbname=$dbname";
        try {
            $this->conn = new PDO($dsn, $username, $password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    public function __destruct()
    {
        $this->conn = null;
    }

    public function query($sql)
    {
        $this->conn->query($sql);
    }

    /**
     * @param string $table
     * @param array $data
     * @return bool|string
     */
    public function insert(string $table, array $data)
    {
        $columns = implode(", ", array_keys($data));
        $values = ":" . implode(", :", array_keys($data));
        $query = "INSERT INTO $table ($columns) VALUES ($values)";

        $stmt = $this->conn->prepare($query);
        $stmt->execute($data);

        return $this->conn->lastInsertId();
    }

    /**
     * @param string $table
     * @param array $data
     * @param string|null $where
     * @return int
     */
    public function update(string $table, array $data, string $where = null)
    {
        $set = "";
        foreach ($data as $key => $value) {
            $set .= "$key=:$key, ";
        }
        $set = rtrim($set, ", ");

        $query = "UPDATE $table SET $set";

        if ($where) {
            $query .= " WHERE $where";
        }

        $stmt = $this->conn->prepare($query);
        $stmt->execute($data);

        return $stmt->rowCount();
    }

    /**
     * @param string $table
     * @param string $where
     * @return int
     */
    public function delete(string $table, string $where)
    {
        $query = "DELETE FROM $table WHERE $where";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * @param string $table
     * @param string|null $where
     * @return bool|array
     */
    public function get(string $table, string $where = null)
    {
        $query = "SELECT * FROM $table";
        $data = [];

        if ($where) {
            $query .= " WHERE $where";
        }
        $stmt = $this->conn->prepare($query);
        $stmt->execute($data);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


//    public function fetch()
//    {
//        $query = "SELECT `COLUMN_NAME`
//            FROM `INFORMATION_SCHEMA`.`COLUMNS`
//             WHERE `TABLE_SCHEMA`='test'
//                   AND `TABLE_NAME`='User'
//            AND `COLUMN_NAME`='img'; ";
//        $stm = $this->conn->prepare($query);
//        $stm->execute();
//        return $stm->fetchAll();
//    }
//
//    public function fetch1()
//    {
//        $query1 = "SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME = 'Logs' AND COLUMN_NAME = 'device_id';";
//        $stm1 = $this->conn->prepare($query1);
//        $stm1->execute();
//        return $stm1->fetchAll();
//    }


}