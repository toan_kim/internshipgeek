<?php

namespace App\Model;

use App\Model\Database;

class Device
{
    const TABLE = 'Device';
    protected $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getAll()
    {
        return $this->db->get(self::TABLE);
    }

    public function add($name, $macAddress, $ip, $power)
    {
        $this->db->insert(self::TABLE, [
            'name' => $name,
            'mac_address' => $macAddress,
            'IP' => $ip,
            'power' => $power
        ]);
    }
}