<?php

namespace App\Model;

use App\Model\Database;

class User
{
    const TABLE = 'User';
    protected $db;
    protected $id = 0;
    protected $username = null;
    protected $password = null;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function load()
    {
        $userId = $this->getId();
        $data = $this->db->get(self::TABLE, "Id = $userId");
        
        if ($data && $data[0]) {
            $this->setId($data[0]['Id']);
            $this->setUserName($data[0]['Username']);
            $this->setPassword($data[0]['Password']);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getUserName()
    {
        return $this->username;
    }

    public function setUserName($userName)
    {
        $this->username = $userName;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function create($username, $password)
    {
        $this->db->insert(self::TABLE, ['username' => $username, 'password' => md5($password)]);
    }

    public function getCurrentUser()
    {
        if (empty($_SESSION['user_id'])) {
            return false;
        } else {
            $user = new User();
            $userId = $_SESSION['user_id'];
            $user->setId($userId)->load();
            return $user;
        }
    }

    public function login($username, $password)
    {
        $password = md5($password);
        if ($rs = $this->db->get(self::TABLE, "username = '$username' AND password = '$password'")) {
            $_SESSION['user_id'] = $rs[0]['Id'];
            return $rs[0]['Id'];
        } else {
            return false;
        }
    }
}
