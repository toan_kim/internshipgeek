<script>
    window.tableData = `<?= $app->data['tableData']; ?>`
    window.tableData = JSON.parse(window.tableData);
</script>
<div class="navbar">
    <ul>
        <div class="mobile">
            <li>
                <i class="fa-solid fa-bars">
                </i>
                </a>
            </li>
        </div>
        <li id="user">
            <a href="#" style="column-rule: black">
                <i style="padding: 0 10px" class="fa-solid fa-user"></i>
                Welcome <?= $app->user->getUsername() ?>!
            </a>
        </li>
    </ul>
</div>
<div class="nav_left">
    <ul>
        <li class="mobile">
            <a href="#" style="column-rule: black">
                <i class="fa-solid fa-user">
                </i> Welcome <?= $app->user->getUsername() ?>!
            </a>
        </li>
        <li id="user1" style="margin-top:16px">
            <a href="#">
                <i class="fa-solid fa-user">
                </i>Trang chủ</a>
            </a>
        </li>
        <br>
        <li><a class="active" style="color: blue" href="/">
                <i class="fa-solid fa-bars">
                </i>
                <span class='ml-2'>
                    Dashboard
                </span>
            </a>
        </li>
        <li>
            <a href="/logs">
                <i class="fa-regular fa-clock"> </i>
                Logs
            </a>
        </li>

        <li class="mt-5">
            <a href="/logout/logout">
                 <i class="fa fa-sign-out" aria-hidden="true"></i>
                Logout
            </a>
        </li>

    </ul>
</div>
<br>
<br>
<div class="table-content">
    <div class="header_content">
        <input type="text" placeholder="Search in table..." id="searchField" class="form-control w-25 mb-2">
        <div class='table-js'>
        </div>
    </div>
    <div class="body_content custom-table" style="margin-top: 20px">
        <div class="content_left">
            <div class="chart-container">
                <canvas id="myChart">

                </canvas>
            </div>
        </div>
        <div class="content_right">
            <form id="addDevice">
                <input type="text" class="name" placeholder="Name" name="name" required>
                <input type="text" name="mac_address" class="mac" placeholder="Mac Address" required>
                <p id="error" >Mac Address already exist</p>
                <button style="display: block">ADD DEVICE</button>
            </form>

        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>