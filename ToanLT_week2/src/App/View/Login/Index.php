
    <div class="container">
        <h2>SOIOT SYSTEM</h2>
        <form id="loginForm" method="POST" action="/login">
            <div class="form-group">
                <input type="text" id="username" name="username" placeholder="user name" required>
            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" placeholder="password" required>
            </div>
            <div class="form-group">
                <input type="submit" value="LOGIN">
                <div class="linn">
                    <a href="/account/create">or create new account</a>
                </div>
            </div>
        </form>
        <p id="errorMessage" class="error"></p>
    </div>