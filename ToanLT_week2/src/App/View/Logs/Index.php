<script>
    window.logTableData = `<?= $app->data['tableData']; ?>`
    window.logTableData = JSON.parse(window.logTableData);
</script>

<div class="navbar">
    <ul>
        <div class="mobile">
            <li>
                <i class="fa-solid fa-bars">
                </i>
                </a>
            </li>
        </div>
        <li id="user">
            <a href="#" style="column-rule: black">
                <i style="padding: 0 10px" class="fa-solid fa-user"></i>
                Welcome <?= $app->user->getUsername() ?>
            </a>
        </li>
    </ul>
</div>
<div class="nav_left">
    <ul>
        <li class="mobile">
            <a href="#" style="column-rule: black">
                <i class="fa-solid fa-user">
                </i>
                Welcome <?= $app->user->getUsername() ?>!
            </a>
        </li>
        <li id="user1" style="margin-top:16px">
            <a href="#">
                <i class="fa-solid fa-user">
                </i>Trang chủ</a>
            </a>
        </li>
        <br>
        <li><a href="/">
                <i class="fa-solid fa-bars">
                </i>
                <span class='ml-2'>
                    Dashboard
                </span>
            </a>
        </li>
        <li>
            <a  class="active" href="/logs">
                <i class="fa-regular fa-clock"> </i>
                Logs
            </a>
        </li>

        <li class="mt-5">
            <a href="/logout/logout">
                 <i class="fa fa-sign-out" aria-hidden="true"></i>
                Logout
            </a>
        </li>
    </ul>
</div>
<div class="table-content">
    <div>
        <h2>
            Action logs
        </h2>
    </div>
    <select id="pagination-select">
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="15">15</option>
    </select>
    <input type="text" placeholder="Search in table..." id="searchField" class="form-control w-25 mb-2">
    <div class="table-sortable">
    Table will be rendered here
    </div>
</div>
