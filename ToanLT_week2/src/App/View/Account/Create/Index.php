<div class="container">
        <h2>Create a account</h2>
        <form method="POST" action="/account/create/add">
            <div class="form-group">
                <input type="text" id="username" name="username" placeholder="user name" required>
            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" placeholder="password" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Create">
            </div>
        </form>
        <p id="errorMessage" class="error"></p>
</div>