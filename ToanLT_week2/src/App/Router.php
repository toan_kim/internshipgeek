<?php

namespace App;

class Router
{
    public function dispatch()
    {
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $path = trim($path, '/');

        if (!trim($path)) {
            $path = 'Home';
        }

        $path = explode('/', $path);
        foreach ($path as &$item) {
            $item = ucfirst(strtolower($item));
        }

        $path = implode('\\', $path);

        $controllerClass = 'App\\Controller\\' . $path;
        if (!class_exists($controllerClass)) {
            $controllerClass = $controllerClass . '\\Index';
        }
        
        if (!class_exists($controllerClass)) {
            require_once 'View/Errors/404.php';
            return;
        }

        $controller = new $controllerClass;
        $controller->execute();
    }
}
