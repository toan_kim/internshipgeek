$(".mobile").click(function () {
    $(".nav_left").toggle()
});
$(document).ready(function () {
    var xValues = ["TV", "Washer", "Refrigerator"];
    var yValues = [55, 24, 15];
    var barColors = [
        "#00aba9",
        "#e8c3b9",
        "#1e7145"
    ];
    new Chart("myChart", {
        type: "doughnut",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            title: {
                display: true,
                text: "Power Consumption"
            }
        }
    });
    $('form').submit(function (e) {
        e.preventDefault();
        var name = $('.name').val();
        var index = xValues.indexOf(name);
        if (index !== -1) {

            yValues[index] += 1;
        } else {
            xValues.push(name);
            yValues.push(1);
        }
        var barColors = [
            "#b91d47",
            "#00aba9",
            "#2b5797",
            "#e8c3b9",
            "#1e7145"
        ];
        var tbody = $('#tbody');
        tbody.empty();
        xValues.forEach(function (value) {
            var row = $('<tr></tr>');
            var cell = $('<td></td>').text(value);
            var cel1l = $('<td></td>').text('00:1B:44:11:3A7');
            var cel12 = $('<td></td>').text('127.0.0.1');
            var cel13 = $('<td></td>').text('2021-05-31');
            var cel14 = $('<td></td>').text('50');
            row.append(cell);
            row.append(cel1l);
            row.append(cel12);
            row.append(cel13);
            row.append(cel14);
            tbody.append(row);
        });
        var totalRow = $('<tr></tr>');
        var countCell = $('<td></td>').text('Total  ');
        var additionalCell = $('<td></td>').text('');
        var additionalCell1 = $('<td></td>').text('');
        var additionalCell2 = $('<td></td>').text('');
        var countValueCell = $('<td></td>').attr('colspan', '3').text(xValues.length * 50);
        totalRow.append(countCell);
        totalRow.append(additionalCell);
        totalRow.append(additionalCell1);
        totalRow.append(additionalCell2);
        totalRow.append(countValueCell);
        tbody.append(totalRow);
        new Chart("myChart", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "Power Consumption"
                }
            }
        });
        $('form')[0].reset();

    });
    var tbody = $('#tbody');
    tbody.empty();
    xValues.forEach(function (value) {
        var row = $('<tr></tr>');
        var cell = $('<td></td>').text(value);
        var cel1l = $('<td></td>').text('00:1B:44:11:3A7');
        var cel12 = $('<td></td>').text('127.0.0.1');
        var cel13 = $('<td></td>').text('2021-05-31');
        var cel14 = $('<td></td>').text('50');
        row.append(cell);
        row.append(cel1l);
        row.append(cel12);
        row.append(cel13);
        row.append(cel14);
        tbody.append(row);
    });

    var totalRow = $('<tr></tr>');
    var countCell = $('<td></td>').text('Total  ');
    var additionalCell = $('<td></td>').text('');
    var additionalCell1 = $('<td></td>').text('');
    var additionalCell2 = $('<td></td>').text('');
    var countValueCell = $('<td></td>').attr('colspan', '3').text(xValues.length * 50);
    totalRow.append(countCell);
    totalRow.append(additionalCell);
    totalRow.append(additionalCell1);
    totalRow.append(additionalCell2);
    totalRow.append(countValueCell);
    tbody.append(totalRow);
});
// logs

var data = [
    ["TV1", "00:1B:44:11:3A711", "127.0.0.1", "50"],
    ["TV2", "00:1B:44:11:3A712", "127.0.0.1", "50"],
    ["TV3", "00:1B:44:11:3A713", "127.0.0.1", "50"],
    ["TV4", "00:1B:44:11:3A714", "127.0.0.1", "50"],
    ["TV5", "00:1B:44:11:3A715", "127.0.0.1", "50"],
    ["TV6", "00:1B:44:11:3A716", "127.0.0.1", "50"],
    ["TV8", "00:1B:44:11:3A717", "127.0.0.1", "50"],
    ["TV9", "00:1B:44:11:3A718", "127.0.0.1", "50"],
    ["Total", "", "", "400"]
];
var itemsPerPage = 3;
var currentPage = 1;

function displayData() {
    var startIndex = (currentPage - 1) * itemsPerPage;
    var endIndex = startIndex + itemsPerPage;
    var pageData = data.slice(startIndex, endIndex);
    var tbody = $('#tbody');
    tbody.empty();

    pageData.forEach(function (rowData) {
        var row = $('<tr></tr>');
        rowData.forEach(function (cellData) {
            var cell = $('<td></td>').text(cellData);
            row.append(cell);
        });
        tbody.append(row);
    });
}


function createPaginationLinks() {
    var totalPages = Math.ceil(data.length / itemsPerPage);
    var paginationLinks = $('#pagination-links');
    paginationLinks.empty();
    for (var i = 1; i <= totalPages; i++) {
        var link = $('<a></a>').attr('href', '#').text(i).data('page', i);

        if (i === currentPage) {
            link.addClass('active');
        }
        link.on('click', function (event) {
            event.preventDefault();
            currentPage = parseInt($(this).data('page'));
            displayData();
            createPaginationLinks();
        });
        paginationLinks.append(link);
    }
}

displayData();
createPaginationLinks();


//  search
$('#searchInput').on('click', function () {
    var searchTerm = $(".nameabc").val();
    console.log(searchTerm);
    var tbody = $('#tbody');
    tbody.empty();

    var matchFound = false;

    for (var i = 0; i < data.length; i++) {
        var row = $('<tr></tr>');

        for (var j = 0; j < data[i].length; j++) {
            var cell = $('<td></td>').text(data[i][j]);

            if (data[i][j].toLowerCase().indexOf(searchTerm) !== -1) {
                matchFound = true;
            }
            row.append(cell);
        }
        if (matchFound) {
            tbody.append(row);
        }
    }
    if (!matchFound) {
        var noDataMessage = $('<tr><td colspan="4">Không có dữ liệu</td></tr>');
        tbody.append(noDataMessage);
    }
});
renderData();
$(".mobile").click(function () {
    $(".nav_left").toggle()
});
